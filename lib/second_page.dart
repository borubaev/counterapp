import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  final String index;

  const SecondPage({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text('Тапшырма 2'),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              height: 40,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(7)),
              child: Center(
                  child: Text(
                'Сиз $index жолу бастыныз!',
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w300),
              )),
            ),
            const SizedBox(
              height: 25,
            ),
            Column(
              children: [
                ElevatedButton.icon(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  // child: const Text(
                  //   'Артка чыгуу учун басыныз.',
                  //   style: TextStyle(fontSize: 20),
                  // ))
                  icon: const Icon(Icons.arrow_back),
                  label: const Text('Артка'),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
